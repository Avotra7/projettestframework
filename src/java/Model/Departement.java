/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import annotation.AnnotationMethode;
import java.util.HashMap;
import modelView.ModelView;
/**
 *
 * @author -53188-
 */
public class Departement {
    int idDept;
    String nomDept;

    public int getIdDept() {
        return idDept;
    }

    public void setIdDept(int idDept) {
        this.idDept = idDept;
    }
    @AnnotationMethode(url = "getNomDept.do")
    public String getNomDept() {
        return nomDept;
    }

    @AnnotationMethode(url = "SetNomDept.do")
    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }

    public Departement(int idDept, String nomDept) {
        this.idDept = idDept;
        this.nomDept = nomDept;
    }

    public Departement() {
    }
    @AnnotationMethode(url="listDept.do")
    public ModelView listDept(){
        
        ModelView mv = new ModelView();
        HashMap<String,Object> h = new HashMap<>();
        mv.setPage("dept.jsp");
        mv.setObj(h);
        this.setNomDept("Informatique");
        mv.getObj().put("dept", this.getNomDept());
        return mv;  
    }
    
}
