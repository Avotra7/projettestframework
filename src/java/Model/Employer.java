/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import annotation.AnnotationMethode;

/**
 *
 * @author -53188-
 */

public class Employer {
    int idEmp;
    String empName;

    public int getIdEmp() {
        return idEmp;
    }

    public void setIdEmp(int idEmp) {
        this.idEmp = idEmp;
    }

    public String getEmpName() {
        return empName;
    }
    @AnnotationMethode(url = "SetEmpName.do")
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Employer(int idEmp, String empName) {
        this.idEmp = idEmp;
        this.empName = empName;
    }

    public Employer() {
    }
}
