/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import annotation.AnnotationMethode;
import java.util.HashMap;
import modelView.ModelView;

/**
 *
 * @author -53188-
 */
public class Personne {
    String nom;
    String prenom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String age) {
        this.prenom = age;
    }

    public Personne(String nom,String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public Personne() {
    }
    
    @AnnotationMethode(url="createPersonne.do")
    public ModelView CreatePersonne(){ 
        ModelView mv = new ModelView();
        HashMap<String,Object> h = new HashMap<>();
        mv.setPage("person.jsp");
        mv.setObj(h);
        mv.getObj().put("person",this);
        return mv;  
    }
}
