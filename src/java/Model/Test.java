/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.HashMap;
import modelView.ModelView;
import annotation.AnnotationMethode;
/**
 *
 * @author -53188-
 */
public class Test {
    int id;
    String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Test(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Test() {
    }
    
    @AnnotationMethode(url ="test.do")
    public ModelView nomTest(){
        ModelView mv = new ModelView();
        HashMap<String,Object> h = new HashMap<>();
        mv.setPage("nom.jsp");
        mv.setObj(h);
        this.setNom("rivo");
        
        mv.getObj().put("nom", this.getNom());
        return mv;
    }
}
