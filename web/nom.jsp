<%@page import="Model.Test"%>
<%@page import="java.util.HashMap"%>
<%@page import="modelView.ModelView"%>
<% HashMap<String,Object> liste = (HashMap<String,Object>)request.getAttribute("nom"); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>nom</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Search.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
    <div>
        <h1><% 
            out.print(liste.get("nom"));%></h1>
    </div>
</body>

</html>